import urllib, json
from datetime import datetime as dt
from datetime import timedelta 
import pytz
import os.path
import pickledb
import dateutil.parser
from flask import Flask, render_template
from werkzeug.contrib.fixers import ProxyFix

F = Flask(__name__)

@F.before_first_request
def setup_logging():
    if not F.debug:
        F.logger.addHandler(logging.FileHandler("logs/istomalive.log"))
        F.logger.setLevel(logging.DEBUG)


DB = pickledb.load('example.db', False) 
USER = "Scruddly"
URL = "http://en.lichess.org/api/user/%s/games?nb=1&page=1" % USER
DISPLAY_STRINGS = {
        15: "Yes",
        30: "Yes, probably",
        45: "Probably, yes",
        60: "Probably",
        120: "Most probably",
        240: "Likely",
        480: "Uncertain",
        720: "Unlikely",
        1440: "Not likely, no",
        2880: "Nope",
        5760: "No"
        }

def getGameData():
    response = urllib.urlopen(URL)
    game_data = json.loads(response.read())
    latest_game_timestamp = dt.fromtimestamp(game_data.get('currentPageResults')[0]['timestamp']/1000, pytz.utc)
    return latest_game_timestamp.isoformat()

def setLatestGameDt():
    DB.set('last_checked_game_dt',dt.utcnow().isoformat())
    DB.set('latest_game_dt',getGameData())
    DB.dump()

def refreshTime():
    last_checked = DB.get('last_checked_game_dt')
    if last_checked:
        last_checked_dt = dateutil.parser.parse(last_checked)
        epsilon = dt.utcnow() - last_checked_dt
        if epsilon > timedelta(minutes=14):
            print "checking again"
            setLatestGameDt()
    else: setLatestGameDt()

def setDisplayString():
    refreshTime()
    lgd = DB.get('latest_game_dt')
    now = dt.now(tz=pytz.utc)
    msg_keys = sorted(DISPLAY_STRINGS.keys(), key=int)
    for i in msg_keys:
        delta = now - dateutil.parser.parse(lgd)
 
        if i < ((delta.days * 24 * 60) + delta.seconds/60):
            continue
        else: 
            F.logger.debug("looking up i:%s" % i)
            return DISPLAY_STRINGS[i]

@F.route('/')
def ishealive():
    status = setDisplayString()
    return render_template('template.html', msg=status)

F.wsgi_app = ProxyFix(F.wsgi_app)
if __name__ == "__main__":
    F.run()
"""
EXAMPLE DATA
{
  "nextPage": 2, 
  "maxPerPage": 1, 
  "currentPageResults": [
    {
      "status": "outoftime", 
      "rated": true, 
      "perf": "blitz", 
      "clock": {
        "totalTime": 180, 
        "initial": 180, 
        "increment": 0
      }, 
      "url": "http://lichess.org/xQVJJy9Y/black", 
      "timestamp": 1416094329112, 
      "winner": "black", 
      "variant": "standard", 
      "players": {
        "white": {
          "rating": 1725, 
          "userId": "jursa", 
          "ratingDiff": -10
        }, 
        "black": {
          "rating": 1895, 
          "userId": "glinscott", 
          "ratingDiff": 6
        }
      }, 
      "turns": 70, 
      "speed": "blitz", 
      "id": "xQVJJy9Y"
    }
  ], 
  "nbPages": 38, 
  "currentPage": 1, 
  "nbResults": 38, 
  "previousPage": null
}
"""
